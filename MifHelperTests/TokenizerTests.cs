using FluentAssertions;
using MifHelper.Tokenizer;
using System.Collections.Generic;
using System.IO;
using Xunit;

namespace MifHelperTests;
public class TokenizerTests
{
    private readonly string testMifDocumentString;
    public TokenizerTests()
    {
        testMifDocumentString = File.ReadAllText("Test.mif");
    }

    [Fact]
    public void TokenizeMifStringTest()
    {
        List<MifToken> createdMifTokens = MifTokenizer.Tokenize(testMifDocumentString);
        createdMifTokens.Should().NotBeEmpty();
    }

    [Fact]
    public void ConvertTokensToStringTest()
    {
        List<MifToken> createdMifTokens = MifTokenizer.Tokenize(testMifDocumentString);
        createdMifTokens.Should().NotBeEmpty();
        string generatedMifContentString = MifTokenizer.ConvertTokensToString(createdMifTokens);
        generatedMifContentString.Should().Be(testMifDocumentString);
    }

    [Fact]
    public void MemSavingTest()
    {
        MifToken commaToken = MifTokenFactory.FromTypeAndValue(MifTokenType.Comma, ",");
        commaToken.Should().BeSameAs(MifTokenFactory.CommaToken);
        MifToken tagStartToken = MifTokenFactory.FromTypeAndValue(MifTokenType.TagStart, "<");
        tagStartToken.Should().BeSameAs(MifTokenFactory.TagStartToken);
        MifToken tagEndToken = MifTokenFactory.FromTypeAndValue(MifTokenType.TagEnd, ">");
        tagEndToken.Should().BeSameAs(MifTokenFactory.TagEndToken);
        MifToken bracketStartToken = MifTokenFactory.FromTypeAndValue(MifTokenType.BracketStart, "(");
        bracketStartToken.Should().BeSameAs(MifTokenFactory.BracketStartToken);
        MifToken bracketEndToken = MifTokenFactory.FromTypeAndValue(MifTokenType.BracketEnd, ")");
        bracketEndToken.Should().BeSameAs(MifTokenFactory.BracketEndToken);

        MifToken commentA = MifTokenFactory.FromTypeAndValue(MifTokenType.Comment, "#test comment");
        MifToken commentB = MifTokenFactory.FromTypeAndValue(MifTokenType.Comment, "#test comment");
        commentA.Should().Be(commentA);
        commentA.Should().NotBeSameAs(commentB);

        MifToken identTokenA = MifTokenFactory.FromIndent(5);
        MifToken identTokenB = MifTokenFactory.FromIndent(5);
        identTokenA.Should().BeSameAs(identTokenB);
    }

    [Fact]
    public void TokenizerLineBreakModeTest()
    {
        MifTokenFactory.LineBreakToken = new MifToken(MifTokenType.LineBreak, "\r\n");
        MifTokenFactory.LineBreakToken.Value.Should().Be("\r\n");
        MifTokenFactory.LineBreakToken = new MifToken(MifTokenType.LineBreak, "\r");
        MifTokenFactory.LineBreakToken.Value.Should().Be("\r");
        MifTokenFactory.LineBreakToken = new MifToken(MifTokenType.LineBreak, "\n");
        MifTokenFactory.LineBreakToken.Value.Should().Be("\n");
    }

}