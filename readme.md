**MIF Helper**

Simple library for handling MIF (Maker Interchange Format) files with C#.

**Components:**
1. Tokenizer and token classes
2. Parser and generic node/tag classes
3. Tag resolving system for concrete custom tag classes 
4. High level documents class and query/modify methods


**Easy to use:**

- Tokenizer
```csharp
    string mifString = File.ReadAllText("YourMifDocument.mif");
    List<MifToken> tokens = MifTokenizer.Tokenize(mifString);
```

- Parser
```csharp
    IEnumerable<IMifNode> mifNodes = MifParser.Parse(tokens).ToList();
```

- Documents
```csharp
    string mifString = File.ReadAllText("YourMifDocument.mif");
    MifDocument mifDocument = new MifDocument(mifString);
    IMifTag paraLineMifTag = mifDocument.FindMifNode<IMifTag>(x => x.TagName == "ParaLine");
```

```csharp
    MifDocument mifDocument = new MifDocument();
    mifDocument.Childs.Add(new MIFFileTag(2019));
    mifDocument.Childs.Add(new ParaTag());
    mifDocument.FindMifNodes<ParaTag>().First().Childs.Add(new ParaLineTag());
    StringTag stringTag = new StringTag("Hello World");
    mifDocument.FindMifNodes<ParaLineTag>().First().Childs.Add(stringTag);
    mifDocument.InsertAfter(stringTag, new MifComment("My first MIF file."));
    File.WriteAllText("Test.mif", mifDocument.ConvertToString());
```

*The project is no longer actively developed by me.*
*Merge requests will be reviewed and if useful integrated.*