﻿using MifHelper.Parser;
using MifHelper.Tokenizer;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MifHelper.Tags
{
    public class TextRectTag : IMifTagWithChilds
    {
        public string TagName => "TextRect";
        public MifComment? TagEndComment { get; set; }
        public List<IMifNode> Childs { get; }

        public TextRectTag() => Childs = new List<IMifNode>();

        public TextRectTag(List<MifToken> parameters, List<IMifNode> childs)
        {
            if (parameters.Count > 0) { throw new Exception($"The '{TagName}' tag does not support parameters!"); }
            Childs = childs;
        }

        public override string ToString() => $"<{TagName} {(Childs.Any() ? "..." : "")}>";

        public IEnumerable<MifToken> AsTokens(int level)
        {
            foreach (var token in this.GetTagStartAsTokens(level)) { yield return token; };
            foreach (var token in this.GetChildsAsTokens(level)) { yield return token; };
            foreach (var token in this.GetTagEndAsTokens(level)) { yield return token; };
        }


    }
}
