﻿using MifHelper.Common;
using MifHelper.Enums;
using MifHelper.Parser;
using MifHelper.Tokenizer;
using System.Collections.Generic;
using System.Globalization;

namespace MifHelper.Tags
{
    public class FSizeTag : IMifTag
    {
        public string TagName => "FSize";
        public MifComment? TagEndComment { get; set; }

        public decimal Size { get; set; }
        public MifUnit Unit { get; set; }

        public FSizeTag(List<MifToken> parameters, List<IMifNode> childs)
        {
            this.ValidateParameters(parameters, MifTokenType.DecimalNumber, MifTokenType.Identifier);
            this.ValidateChilds(childs, false);
            Size = decimal.Parse(parameters[0].Value, CultureInfo.InvariantCulture);
            Unit = MifUnitHelper.StringToMifUnit(parameters[1].Value);
        }

        public FSizeTag(string size, MifUnit unit) : this(decimal.Parse(size, CultureInfo.InvariantCulture), unit) { }

        public FSizeTag(decimal size, MifUnit unit)
        {
            Size = size;
            Unit = unit;
        }

        public override string ToString() => $"<{TagName} {Size}{Unit}>";

        public IEnumerable<MifToken> AsTokens(int level)
        {
            foreach (var token in this.GetTagStartAsTokens(level)) { yield return token; };
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromDecimalNumber(Size);
            yield return MifTokenFactory.FromIdentifier(MifUnitHelper.MifUnitToString(Unit));
            foreach (var token in this.GetTagEndAsTokens(level)) { yield return token; };
        }
    }
}
