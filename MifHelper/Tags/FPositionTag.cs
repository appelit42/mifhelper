﻿using MifHelper.Common;
using MifHelper.Parser;
using MifHelper.Tokenizer;
using System.Collections.Generic;

namespace MifHelper.Tags
{
    public class FPositionTag : IMifTag
    {
        public string TagName => "FPosition";
        public MifComment? TagEndComment { get; set; }
        public string Keyword { get; set; }

        public FPositionTag(string keyword) => Keyword = keyword;

        public FPositionTag(List<MifToken> parameters, List<IMifNode> childs)
        {
            this.ValidateParameters(parameters, MifTokenType.Identifier);
            this.ValidateChilds(childs, false);
            Keyword = parameters[0].Value;
        }

        public override string ToString() => $"<{TagName} {Keyword}>";

        public IEnumerable<MifToken> AsTokens(int level)
        {
            foreach (var token in this.GetTagStartAsTokens(level)) { yield return token; };
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromIdentifier(Keyword);
            foreach (var token in this.GetTagEndAsTokens(level)) { yield return token; };
        }
    }
}
