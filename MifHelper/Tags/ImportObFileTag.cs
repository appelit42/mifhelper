﻿using MifHelper.Common;
using MifHelper.Parser;
using MifHelper.Tokenizer;
using System.Collections.Generic;

namespace MifHelper.Tags
{
    public class ImportObFileTag : IMifTag
    {
        public string TagName => "ImportObFile";
        public MifComment? TagEndComment { get; set; }
        public string Path { get; set; }

        public ImportObFileTag(string path) => Path = path;

        public ImportObFileTag(List<MifToken> parameters, List<IMifNode> childs)
        {
            this.ValidateParameters(parameters, MifTokenType.String);
            this.ValidateChilds(childs, false);
            Path = parameters[0].Value.TrimStart('`').TrimEnd('\'');
        }

        public override string ToString() => $"<{TagName} `{CreatePathValue(Path)}'>";

        public IEnumerable<MifToken> AsTokens(int level)
        {
            foreach (var token in this.GetTagStartAsTokens(level)) { yield return token; };
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromString(CreatePathValue(Path));
            foreach (var token in this.GetTagEndAsTokens(level)) { yield return token; };
        }

        private string CreatePathValue(string value)
        {
            return value.Replace('\\', '/');
        }
    }
}
