﻿using MifHelper.Parser;
using MifHelper.Tokenizer;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MifHelper.Tags
{
    public class ImportObjectTag : IMifTagWithChilds
    {
        public string TagName => "ImportObject";
        public MifComment? TagEndComment { get; set; }
        public List<IMifNode> Childs { get; }

        public ImportObjectTag() => Childs = new List<IMifNode>();

        public ImportObjectTag(List<MifToken> parameters, List<IMifNode> childs)
        {
            if (parameters.Count > 0) { throw new Exception($"The '{TagName}' tag does not support parameters!"); }
            Childs = childs;
        }

        public override string ToString() => $"<{TagName} {(Childs.Any() ? "..." : "")}>";

        public IEnumerable<MifToken> AsTokens(int level)
        {
            foreach (var token in this.GetTagStartAsTokens(level)) { yield return token; };
            foreach (var token in this.GetChildsAsTokens(level)) { yield return token; };
            foreach (var token in this.GetTagEndAsTokens(level)) { yield return token; };
        }


    }
}
