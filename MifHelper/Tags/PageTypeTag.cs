﻿using MifHelper.Common;
using MifHelper.Parser;
using MifHelper.Tokenizer;
using System.Collections.Generic;

namespace MifHelper.Tags
{
    /// <summary>
    /// Includes an extended ASCII character:
    /// Examples: Tab, HardSpace, SoftHyphen, HardHyphen, DiscHyphen, NoHyphen, Cent, Pound, Yen, EnDash, EmDash, Dagger, DoubleDagger, Bullet, HardReturn, NumberSpace, ThinSpace, EnSpace, EmSpace
    /// 
    /// In MIF versions >= 8 documents, special characters like the following can directly entered with the UTF-8 code of these characters: 
    /// DiscHyphen, NoHyphen, HardHyphen, Tab, HardReturn, NumberSpace, HardSpace, ThinSpace, EnSpace, EmSpace, ...
    /// </summary>
    public class PageTypeTag : IMifTag
    {
        public string TagName => "PageType";
        public MifComment? TagEndComment { get; set; }
        public string Type { get; set; }

        public PageTypeTag(string type) => Type = type;

        public PageTypeTag(List<MifToken> parameters, List<IMifNode> childs)
        {
            this.ValidateParameters(parameters, MifTokenType.Identifier);
            this.ValidateChilds(childs, false);
            Type = parameters[0].Value;
        }

        public override string ToString() => $"<{TagName} {Type}>";

        public IEnumerable<MifToken> AsTokens(int level)
        {
            foreach (var token in this.GetTagStartAsTokens(level)) { yield return token; };
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromIdentifier(Type);
            foreach (var token in this.GetTagEndAsTokens(level)) { yield return token; };
        }
    }
}
