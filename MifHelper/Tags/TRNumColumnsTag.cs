﻿using MifHelper.Common;
using MifHelper.Parser;
using MifHelper.Tokenizer;
using System.Collections.Generic;
using System.Globalization;

namespace MifHelper.Tags
{
    public class TRNumColumnsTag : IMifTag
    {
        public string TagName => "TRNumColumns";
        public MifComment? TagEndComment { get; set; }
        public int Value { get; set; }

        public TRNumColumnsTag(int value) => Value = value;

        public TRNumColumnsTag(List<MifToken> parameters, List<IMifNode> childs)
        {
            this.ValidateParameters(parameters, MifTokenType.Number);
            this.ValidateChilds(childs, false);
            Value = int.Parse(parameters[0].Value, CultureInfo.InvariantCulture);
        }

        public override string ToString() => $"<{TagName} {Value}>";

        public IEnumerable<MifToken> AsTokens(int level)
        {
            foreach (var token in this.GetTagStartAsTokens(level)) { yield return token; };
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromNumber(Value);
            foreach (var token in this.GetTagEndAsTokens(level)) { yield return token; };
        }
    }
}
