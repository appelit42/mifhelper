﻿using MifHelper.Common;
using MifHelper.Enums;
using MifHelper.Parser;
using MifHelper.Tokenizer;
using System.Collections.Generic;
using System.Globalization;

namespace MifHelper.Tags
{
    public class PenWidthTag : IMifTag
    {
        public string TagName => "PenWidth";
        public MifComment? TagEndComment { get; set; }

        public decimal Width { get; set; }
        public MifUnit Unit { get; set; }

        public PenWidthTag(List<MifToken> parameters, List<IMifNode> childs)
        {
            this.ValidateParameters(parameters, MifTokenType.DecimalNumber, MifTokenType.Identifier);
            this.ValidateChilds(childs, false);
            Width = decimal.Parse(parameters[0].Value, CultureInfo.InvariantCulture);
            Unit = MifUnitHelper.StringToMifUnit(parameters[1].Value);
        }

        public PenWidthTag(string width, MifUnit unit) : this(decimal.Parse(width, CultureInfo.InvariantCulture), unit) { }

        public PenWidthTag(decimal width, MifUnit unit)
        {
            Width = width;
            Unit = unit;
        }

        public override string ToString() => $"<{TagName} {Width}{Unit}>";

        public IEnumerable<MifToken> AsTokens(int level)
        {
            foreach (var token in this.GetTagStartAsTokens(level)) { yield return token; };
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromDecimalNumber(Width);
            yield return MifTokenFactory.FromIdentifier(MifUnitHelper.MifUnitToString(Unit));
            foreach (var token in this.GetTagEndAsTokens(level)) { yield return token; };
        }
    }
}
