﻿using MifHelper.Common;
using MifHelper.Parser;
using MifHelper.Tokenizer;
using System;
using System.Collections.Generic;

namespace MifHelper.Tags
{
    public class FLockedTag : IMifTag
    {
        public string TagName => "FLocked";
        public MifComment? TagEndComment { get; set; }
        public bool Value { get; set; }

        public FLockedTag(bool value) => Value = value;

        public FLockedTag(List<MifToken> parameters, List<IMifNode> childs)
        {
            this.ValidateParameters(parameters, MifTokenType.Identifier);
            this.ValidateChilds(childs, false);
            Value = parameters[0].Value.ToLower() switch
            {
                "yes" => true,
                "no" => false,
                _ => throw new AggregateException($"The '{TagName}' tag requires a boolean (Yes/No) parameter!")
            };
        }

        public override string ToString() => $"<{TagName} {Value}>";

        public IEnumerable<MifToken> AsTokens(int level)
        {
            foreach (var token in this.GetTagStartAsTokens(level)) { yield return token; };
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromIdentifier(Value ? "Yes" : "No");
            foreach (var token in this.GetTagEndAsTokens(level)) { yield return token; };
        }
    }
}
