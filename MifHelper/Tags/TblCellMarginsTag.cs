﻿using MifHelper.Common;
using MifHelper.Enums;
using MifHelper.Parser;
using MifHelper.Tokenizer;
using System.Collections.Generic;
using System.Globalization;

namespace MifHelper.Tags
{
    public class TblCellMarginsTag : IMifTag
    {
        public string TagName => "TblCellMargins";
        public MifComment? TagEndComment { get; set; }

        public decimal Left { get; set; }
        public decimal Top { get; set; }
        public decimal Right { get; set; }
        public decimal Bottom { get; set; }
        public MifUnit LeftUnit { get; set; }
        public MifUnit TopUnit { get; set; }
        public MifUnit RightUnit { get; set; }
        public MifUnit BottomUnit { get; set; }

        public TblCellMarginsTag(List<MifToken> parameters, List<IMifNode> childs)
        {
            this.ValidateValueWithUnitParameters(parameters, 4);
            this.ValidateChilds(childs, false);
            Left = decimal.Parse(parameters[0].Value, CultureInfo.InvariantCulture);
            LeftUnit = MifUnitHelper.StringToMifUnit(parameters[1].Value);
            Top = decimal.Parse(parameters[2].Value, CultureInfo.InvariantCulture);
            TopUnit = MifUnitHelper.StringToMifUnit(parameters[3].Value);
            Right = decimal.Parse(parameters[4].Value, CultureInfo.InvariantCulture);
            RightUnit = MifUnitHelper.StringToMifUnit(parameters[5].Value);
            Bottom = decimal.Parse(parameters[6].Value, CultureInfo.InvariantCulture);
            BottomUnit = MifUnitHelper.StringToMifUnit(parameters[7].Value);
        }

        public TblCellMarginsTag(string left, string top, string right, string bottom, MifUnit unit) :
            this(left, top, right, bottom, unit, CultureInfo.InvariantCulture)
        { }

        public TblCellMarginsTag(string left, string top, string right, string bottom, MifUnit unit, CultureInfo c) :
            this(decimal.Parse(left, c), decimal.Parse(top, c), decimal.Parse(right, c), decimal.Parse(bottom, c), unit)
        { }

        public TblCellMarginsTag(decimal left, decimal top, decimal right, decimal bottom, MifUnit unit)
        {
            Left = left;
            LeftUnit = unit;
            Top = top;
            TopUnit = unit;
            Right = right;
            RightUnit = unit;
            Bottom = bottom;
            BottomUnit = unit;
        }

        public override string ToString() => $"<{TagName} {Left} {LeftUnit} {Top} {TopUnit} {Right} {RightUnit} {Bottom} {BottomUnit}>";

        public IEnumerable<MifToken> AsTokens(int level)
        {
            foreach (var token in this.GetTagStartAsTokens(level)) { yield return token; };
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromNumber(Left);
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromIdentifier(MifUnitHelper.MifUnitToString(LeftUnit));
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromNumber(Top);
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromIdentifier(MifUnitHelper.MifUnitToString(TopUnit));
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromNumber(Right);
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromIdentifier(MifUnitHelper.MifUnitToString(RightUnit));
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromNumber(Bottom);
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromIdentifier(MifUnitHelper.MifUnitToString(BottomUnit));
            foreach (var token in this.GetTagEndAsTokens(level)) { yield return token; };
        }
    }
}
