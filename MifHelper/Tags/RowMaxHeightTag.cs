﻿using MifHelper.Common;
using MifHelper.Enums;
using MifHelper.Parser;
using MifHelper.Tokenizer;
using System.Collections.Generic;
using System.Globalization;

namespace MifHelper.Tags
{
    public class RowMaxHeightTag : IMifTag
    {
        public string TagName => "RowMaxHeight";
        public MifComment? TagEndComment { get; set; }

        public decimal Height { get; set; }
        public MifUnit Unit { get; set; }

        public RowMaxHeightTag(List<MifToken> parameters, List<IMifNode> childs)
        {
            this.ValidateParameters(parameters, MifTokenType.DecimalNumber, MifTokenType.Identifier);
            this.ValidateChilds(childs, false);

            Height = decimal.Parse(parameters[0].Value, CultureInfo.InvariantCulture);
            Unit = MifUnitHelper.StringToMifUnit(parameters[1].Value);
        }

        public RowMaxHeightTag(string height, MifUnit unit) :
            this(height, unit, CultureInfo.InvariantCulture)
        { }

        public RowMaxHeightTag(string height, MifUnit unit, CultureInfo c) :
            this(decimal.Parse(height, c), unit)
        { }

        public RowMaxHeightTag(decimal height, MifUnit unit)
        {
            Height = height;
            Unit = unit;
        }

        public override string ToString() => $"<{TagName} {Height} {Unit}>";

        public IEnumerable<MifToken> AsTokens(int level)
        {
            foreach (var token in this.GetTagStartAsTokens(level)) { yield return token; };
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromNumber(Height);
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromIdentifier(MifUnitHelper.MifUnitToString(Unit));
            foreach (var token in this.GetTagEndAsTokens(level)) { yield return token; };
        }
    }
}
