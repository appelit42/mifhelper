﻿using MifHelper.Common;
using MifHelper.Enums;
using MifHelper.Parser;
using MifHelper.Tokenizer;
using System;
using System.Collections.Generic;

namespace MifHelper.Tags
{
    public class StartPageSideTag : IMifTag
    {
        public string TagName => "StartPageSide";
        public MifComment? TagEndComment { get; set; }
        public StartPageSides Value { get; set; }

        public StartPageSideTag(StartPageSides value) => Value = value;

        public StartPageSideTag(List<MifToken> parameters, List<IMifNode> childs)
        {
            this.ValidateParameters(parameters, MifTokenType.Identifier);
            this.ValidateChilds(childs, false);
            if (Enum.TryParse(parameters[0].Value, out StartPageSides startPageSides))
            {
                Value = startPageSides;
            }
        }

        public override string ToString() => $"<{TagName} {Value}>";

        public IEnumerable<MifToken> AsTokens(int level)
        {
            foreach (var token in this.GetTagStartAsTokens(level)) { yield return token; };
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromIdentifier(Value.ToString());
            foreach (var token in this.GetTagEndAsTokens(level)) { yield return token; };
        }
    }
}
