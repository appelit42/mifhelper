﻿using MifHelper.Common;
using MifHelper.Parser;
using MifHelper.Tokenizer;
using System.Collections.Generic;
using System.Globalization;

namespace MifHelper.Tags
{
    public class MTypeTag : IMifTag
    {
        public string TagName => "MType";
        public MifComment? TagEndComment { get; set; }
        public int Type { get; set; }

        public MTypeTag(int type) => Type = type;

        public MTypeTag(List<MifToken> parameters, List<IMifNode> childs)
        {
            this.ValidateParameters(parameters, MifTokenType.Number);
            this.ValidateChilds(childs, false);
            Type = int.Parse(parameters[0].Value, CultureInfo.InvariantCulture);
        }

        public override string ToString() => $"<{TagName} {Type}>";

        public IEnumerable<MifToken> AsTokens(int level)
        {
            foreach (var token in this.GetTagStartAsTokens(level)) { yield return token; };
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromNumber(Type);
            foreach (var token in this.GetTagEndAsTokens(level)) { yield return token; };
        }
    }

}
