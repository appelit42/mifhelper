﻿using MifHelper.Common;
using MifHelper.Parser;
using MifHelper.Tokenizer;
using System.Collections.Generic;

namespace MifHelper.Tags
{
    /// <summary>
    /// Includes an extended ASCII character:
    /// Examples: Tab, HardSpace, SoftHyphen, HardHyphen, DiscHyphen, NoHyphen, Cent, Pound, Yen, EnDash, EmDash, Dagger, DoubleDagger, Bullet, HardReturn, NumberSpace, ThinSpace, EnSpace, EmSpace
    /// 
    /// In MIF versions >= 8 documents, special characters like the following can directly entered with the UTF-8 code of these characters: 
    /// DiscHyphen, NoHyphen, HardHyphen, Tab, HardReturn, NumberSpace, HardSpace, ThinSpace, EnSpace, EmSpace, ...
    /// </summary>
    public class CharTag : IMifTag
    {
        public string TagName => "Char";
        public MifComment? TagEndComment { get; set; }
        public string Keyword { get; set; }

        public CharTag(string keyword) => Keyword = keyword;

        public CharTag(List<MifToken> parameters, List<IMifNode> childs)
        {
            this.ValidateParameters(parameters, MifTokenType.Identifier);
            this.ValidateChilds(childs, false);
            Keyword = parameters[0].Value;
        }

        public override string ToString() => $"<{TagName} {Keyword}>";

        public IEnumerable<MifToken> AsTokens(int level)
        {
            foreach (var token in this.GetTagStartAsTokens(level)) { yield return token; };
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromIdentifier(Keyword);
            foreach (var token in this.GetTagEndAsTokens(level)) { yield return token; };
        }
    }
}
