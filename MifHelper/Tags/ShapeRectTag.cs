﻿using MifHelper.Common;
using MifHelper.Enums;
using MifHelper.Parser;
using MifHelper.Tokenizer;
using System.Collections.Generic;
using System.Globalization;

namespace MifHelper.Tags
{
    public class ShapeRectTag : IMifTag
    {
        public string TagName => "ShapeRect";
        public MifComment? TagEndComment { get; set; }

        public decimal Left { get; set; }
        public decimal Top { get; set; }
        public decimal Width { get; set; }
        public decimal Height { get; set; }
        public MifUnit LeftUnit { get; set; }
        public MifUnit TopUnit { get; set; }
        public MifUnit WidthUnit { get; set; }
        public MifUnit HeightUnit { get; set; }

        public ShapeRectTag(List<MifToken> parameters, List<IMifNode> childs)
        {
            this.ValidateValueWithUnitParameters(parameters, 4);
            this.ValidateChilds(childs, false);

            Left = decimal.Parse(parameters[0].Value, CultureInfo.InvariantCulture);
            LeftUnit = MifUnitHelper.StringToMifUnit(parameters[1].Value);
            Top = decimal.Parse(parameters[2].Value, CultureInfo.InvariantCulture);
            TopUnit = MifUnitHelper.StringToMifUnit(parameters[3].Value);
            Width = decimal.Parse(parameters[4].Value, CultureInfo.InvariantCulture);
            WidthUnit = MifUnitHelper.StringToMifUnit(parameters[5].Value);
            Height = decimal.Parse(parameters[6].Value, CultureInfo.InvariantCulture);
            HeightUnit = MifUnitHelper.StringToMifUnit(parameters[7].Value);
        }

        public ShapeRectTag(string xPosition, string yPosition, string width, string height, MifUnit unit) :
            this(xPosition, yPosition, width, height, unit, CultureInfo.InvariantCulture)
        { }

        public ShapeRectTag(string xPosition, string yPosition, string width, string height, MifUnit unit, CultureInfo c) :
            this(decimal.Parse(xPosition, c), decimal.Parse(yPosition, c), decimal.Parse(width, c), decimal.Parse(height, c), unit)
        { }

        public ShapeRectTag(decimal xPosition, decimal yPosition, decimal width, decimal height, MifUnit unit)
        {
            Left = xPosition;
            LeftUnit = unit;
            Top = yPosition;
            TopUnit = unit;
            Width = width;
            WidthUnit = unit;
            Height = height;
            HeightUnit = unit;
        }

        public override string ToString() => $"<{TagName} {Left} {LeftUnit} {Top} {TopUnit} {Width} {WidthUnit} {Height} {HeightUnit}>";

        public IEnumerable<MifToken> AsTokens(int level)
        {
            foreach (var token in this.GetTagStartAsTokens(level)) { yield return token; };
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromNumber(Left);
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromIdentifier(MifUnitHelper.MifUnitToString(LeftUnit));
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromNumber(Top);
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromIdentifier(MifUnitHelper.MifUnitToString(TopUnit));
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromNumber(Width);
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromIdentifier(MifUnitHelper.MifUnitToString(WidthUnit));
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromNumber(Height);
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromIdentifier(MifUnitHelper.MifUnitToString(HeightUnit));
            foreach (var token in this.GetTagEndAsTokens(level)) { yield return token; };
        }
    }
}
