﻿using MifHelper.Common;
using MifHelper.Enums;
using MifHelper.Parser;
using MifHelper.Tokenizer;
using System.Collections.Generic;
using System.Globalization;

namespace MifHelper.Tags
{
    public class TblColumnWidthTag : IMifTag
    {
        public string TagName => "TblColumnWidth";
        public MifComment? TagEndComment { get; set; }

        public decimal Width { get; set; }
        public MifUnit Unit { get; set; }

        public TblColumnWidthTag(List<MifToken> parameters, List<IMifNode> childs)
        {
            this.ValidateParameters(parameters, MifTokenType.DecimalNumber, MifTokenType.Identifier);
            this.ValidateChilds(childs, false);

            Width = decimal.Parse(parameters[0].Value, CultureInfo.InvariantCulture);
            Unit = MifUnitHelper.StringToMifUnit(parameters[1].Value);
        }

        public TblColumnWidthTag(string width, MifUnit unit) :
            this(width, unit, CultureInfo.InvariantCulture)
        { }

        public TblColumnWidthTag(string width, MifUnit unit, CultureInfo c) :
            this(decimal.Parse(width, c), unit)
        { }

        public TblColumnWidthTag(decimal width, MifUnit unit)
        {
            Width = width;
            Unit = unit;
        }

        public override string ToString() => $"<{TagName} {Width} {Unit}>";

        public IEnumerable<MifToken> AsTokens(int level)
        {
            foreach (var token in this.GetTagStartAsTokens(level)) { yield return token; };
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromNumber(Width);
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromIdentifier(MifUnitHelper.MifUnitToString(Unit));
            foreach (var token in this.GetTagEndAsTokens(level)) { yield return token; };
        }
    }
}
