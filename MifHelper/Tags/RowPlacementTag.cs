﻿using MifHelper.Common;
using MifHelper.Parser;
using MifHelper.Tokenizer;
using System;
using System.Collections.Generic;

namespace MifHelper.Tags
{
    public class RowPlacementTag : IMifTag
    {
        public string TagName => "RowPlacement";
        public MifComment? TagEndComment { get; set; }
        public string Keyword { get; set; }

        public RowPlacementTag(string keyword) => Keyword = keyword;

        public RowPlacementTag(List<MifToken> parameters, List<IMifNode> childs)
        {
            this.ValidateParameters(parameters, MifTokenType.Identifier);
            this.ValidateChilds(childs, false);
            Keyword = parameters[0].Value.ToLower() switch
            {
                "anywhere" => "Anywhere",
                "columntop" => "ColumnTop",
                "lpagetop" => "LPageTop",
                "rpagetop" => "RPageTop",
                "pagetop" => "PageTop",
                _ => throw new AggregateException($"The '{TagName}' tag requires a alignment ('Anywhere', 'ColumnTop', 'LPageTop', 'RPageTop' or 'PageTop') as parameter!")
            };
        }

        public override string ToString() => $"<{TagName} {Keyword}>";

        public IEnumerable<MifToken> AsTokens(int level)
        {
            foreach (var token in this.GetTagStartAsTokens(level)) { yield return token; };
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromIdentifier(Keyword);
            foreach (var token in this.GetTagEndAsTokens(level)) { yield return token; };
        }
    }
}