﻿using MifHelper.Common;
using MifHelper.Enums;
using MifHelper.Parser;
using MifHelper.Tokenizer;
using System.Collections.Generic;
using System.Globalization;

namespace MifHelper.Tags
{
    public class PointTag : IMifTag
    {
        public string TagName => "Point";
        public MifComment? TagEndComment { get; set; }

        public decimal X { get; set; }
        public decimal Y { get; set; }
        public MifUnit XUnit { get; set; }
        public MifUnit YUnit { get; set; }

        public PointTag(List<MifToken> parameters, List<IMifNode> childs)
        {
            this.ValidateValueWithUnitParameters(parameters, 2);
            this.ValidateChilds(childs, false);

            X = decimal.Parse(parameters[0].Value, CultureInfo.InvariantCulture);
            XUnit = MifUnitHelper.StringToMifUnit(parameters[1].Value);
            Y = decimal.Parse(parameters[2].Value, CultureInfo.InvariantCulture);
            YUnit = MifUnitHelper.StringToMifUnit(parameters[3].Value);
        }

        public PointTag(string x, string y, MifUnit unit) :
            this(x, y, unit, CultureInfo.InvariantCulture)
        { }

        public PointTag(string x, string y, MifUnit unit, CultureInfo c) :
            this(decimal.Parse(x, c), decimal.Parse(y, c), unit)
        { }

        public PointTag(decimal x, decimal y, MifUnit unit)
        {
            X = x;
            XUnit = unit;
            Y = y;
            YUnit = unit;
        }

        public override string ToString() => $"<{TagName} {X.ToString(CultureInfo.InvariantCulture)} {XUnit} {Y} {YUnit}>";

        public IEnumerable<MifToken> AsTokens(int level)
        {
            foreach (var token in this.GetTagStartAsTokens(level)) { yield return token; };
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromNumber(X);
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromIdentifier(MifUnitHelper.MifUnitToString(XUnit));
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromNumber(Y);
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromIdentifier(MifUnitHelper.MifUnitToString(YUnit));
            foreach (var token in this.GetTagEndAsTokens(level)) { yield return token; };
        }
    }
}
