﻿using MifHelper.Common;
using MifHelper.Parser;
using MifHelper.Tokenizer;
using System.Collections.Generic;
using System.Globalization;

namespace MifHelper.Tags
{
    public class AngleTag : IMifTag
    {
        public string TagName => "Angle";
        public MifComment? TagEndComment { get; set; }

        public decimal Angle { get; set; }

        public AngleTag(List<MifToken> parameters, List<IMifNode> childs)
        {
            this.ValidateParameters(parameters, MifTokenType.DecimalNumber);
            this.ValidateChilds(childs, false);
            Angle = decimal.Parse(parameters[0].Value, CultureInfo.InvariantCulture);
        }

        public AngleTag(string angle) : this(decimal.Parse(angle, CultureInfo.InvariantCulture)) { }

        public AngleTag(decimal angle)
        {
            Angle = angle;
        }

        public override string ToString() => $"<{TagName} {Angle}>";

        public IEnumerable<MifToken> AsTokens(int level)
        {
            foreach (var token in this.GetTagStartAsTokens(level)) { yield return token; };
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromDecimalNumber(Angle);
            foreach (var token in this.GetTagEndAsTokens(level)) { yield return token; };
        }
    }
}
