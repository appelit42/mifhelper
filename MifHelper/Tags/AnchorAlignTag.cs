﻿using MifHelper.Common;
using MifHelper.Enums;
using MifHelper.Parser;
using MifHelper.Tokenizer;
using System;
using System.Collections.Generic;

namespace MifHelper.Tags
{
    public class AnchorAlignTag : IMifTag
    {
        public string TagName => "AnchorAlign";
        public MifComment? TagEndComment { get; set; }
        public AnchorAlign Value { get; set; }

        public AnchorAlignTag(AnchorAlign value) => Value = value;

        public AnchorAlignTag(List<MifToken> parameters, List<IMifNode> childs)
        {
            this.ValidateParameters(parameters, MifTokenType.Identifier);
            this.ValidateChilds(childs, false);
            if (Enum.TryParse(parameters[0].Value, out AnchorAlign frameType))
            {
                Value = frameType;
            }
        }

        public override string ToString() => $"<{TagName} {Value}>";

        public IEnumerable<MifToken> AsTokens(int level)
        {
            foreach (var token in this.GetTagStartAsTokens(level)) { yield return token; };
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromIdentifier(Value.ToString());
            foreach (var token in this.GetTagEndAsTokens(level)) { yield return token; };
        }
    }
}
