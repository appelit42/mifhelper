﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace MifHelper.Common
{
    public static class MifPathHelper
    {
        private static readonly Regex pathComponentRegex = new(@"(<[v|c|u])[\s]*\\>", RegexOptions.Compiled);

        public static string EncodePath(string rawPath)
        {
            StringBuilder sb = new();
            Stack<string> stack = new();
            var path = rawPath;
            while (true)
            {
                var fileName = Path.GetFileName(path);
                if (!string.IsNullOrEmpty(fileName)) { stack.Push(fileName); }
                path = Path.GetDirectoryName(path);
                if (string.IsNullOrEmpty(path?.TrimEnd(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar)))
                {
                    fileName = Path.GetPathRoot(rawPath)?.TrimEnd(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                    if (!string.IsNullOrEmpty(fileName)) { stack.Push(fileName.TrimEnd('\\')); }
                    break;
                }
            }
            foreach (var item in stack)
            {
                switch (item)
                {
                    case ".":
                        break;
                    case "..":
                        sb.Append(@"<u\>");
                        break;
                    default:
                        sb.Append(@$"<{(Path.IsPathRooted(item) ? "v" : "c")}\>");
                        sb.Append(MifStringHelper.EncodeString(item));
                        break;
                }
            }
            return sb.ToString();
        }

        public static string DecodePath(string encodedPath)
        {
            StringBuilder sb = new();
            var parts = pathComponentRegex.Split(encodedPath);
            foreach (var part in parts)
            {
                if (string.IsNullOrEmpty(part)) { continue; }
                sb.Append(part switch
                {
                    "<v" => @"",
                    "<c" => sb.Length > 0 ? @"\" : @"",
                    "<u" => @"..",
                    _ => MifStringHelper.DecodeString(part),
                });
            }
            return sb.ToString();
        }

    }
}
