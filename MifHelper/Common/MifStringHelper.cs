﻿using System.Globalization;
using System.Text;

namespace MifHelper.Common
{
    public static class MifStringHelper
    {
        /// <summary>
        /// Escape unicode chars for non UTF-8 output.
        /// </summary>
        public static bool EscapeUnicodeChars { get; set; } = false;

        /// <summary>
        /// Escape non-ASCII chars for the legacy MIF-Classic format (with \x) instead for the new MIF-Unicode format (\u).
        /// MIF-Classic FrameMaker 1.0 - 7.0 
        /// MIF-Unicode FrameMaker 8.0 - 2019
        /// </summary>
        public static bool UseOldEscapeMode { get; set; } = false;

        private static readonly FrameRomanEncoding frameRomanEncoding = new();

        public static string EncodeString(string rawString)
        {
            if (string.IsNullOrEmpty(rawString)) { return ""; }
            StringBuilder sb = new();
            foreach (char currentChar in rawString)
            {
                sb.Append(currentChar switch
                {
                    '\t' => @"\t",
                    '>' => @"\>",
                    '\'' => @"\q",
                    '`' => @"\Q",
                    '\\' => @"\\",
                    _ => currentChar <= 127 || !EscapeUnicodeChars ? currentChar.ToString() : EscapeUnicodeChar(currentChar)
                });
            }
            return sb.ToString();
        }

        private static string EscapeUnicodeChar(char unicodeChar)
        {
            var escaped = @$"\u{((int)unicodeChar).ToString("X").PadLeft(4, '0')} ";
            if (UseOldEscapeMode)
            {
                escaped = frameRomanEncoding.ToFrameRoman(escaped.Trim()) + " ";
            }
            return escaped;
        }

        public static string DecodeString(string encodedString)
        {
            if (string.IsNullOrEmpty(encodedString)) { return ""; }
            StringBuilder sb = new(encodedString);
            for (int i = 0; i < sb.Length - 1; i++)
            {
                if (sb[i] != '\\') { continue; }
                switch (sb[i + 1])
                {
                    case 't':
                        sb[i + 1] = '\t';
                        sb.Remove(i, 1);
                        break;
                    case '>':
                        sb.Remove(i, 1);
                        break;
                    case 'q':
                        sb[i + 1] = '\'';
                        sb.Remove(i, 1);
                        break;
                    case 'Q':
                        sb[i + 1] = '`';
                        sb.Remove(i, 1);
                        break;
                    case '\\':
                        sb.Remove(i, 1);
                        break;
                    case 'x':
                        sb[i] = (char)int.Parse(frameRomanEncoding.ToUnicode(sb.ToString(i, 4)).Substring(2, 4), NumberStyles.HexNumber);
                        sb.Remove(i + 1, 4);
                        break;
                    case 'u':
                        sb[i] = (char)int.Parse(sb.ToString(i + 2, 4), NumberStyles.HexNumber);
                        sb.Remove(i + 1, 6);
                        break;
                }
            }
            return sb.ToString();
        }

    }
}
