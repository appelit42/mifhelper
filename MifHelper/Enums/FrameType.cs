﻿namespace MifHelper.Enums
{
    public enum FrameType
    {
        Below,
        Top,
        Bottom,
        Inline,
        Left,
        Right,
        Inside,
        Outside,
        Near,
        Far,
        RunIntoParagraph,
        NotAnchored
    }
}
