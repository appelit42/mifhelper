﻿namespace MifHelper.Enums
{
    public enum AnchorAlign
    {
        Left,
        Right,
        Inside,
        Outside,
        Center,
    }
}
