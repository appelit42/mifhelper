﻿namespace MifHelper.Enums
{
    public enum MifUnit
    {
        Point,
        Inch,
        Millimeter,
        Centimeter,
        Pica,
        Didot,
        Cicero,
        Pixel
    }
}
