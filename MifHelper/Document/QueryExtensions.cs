﻿using MifHelper.Parser;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MifHelper.Document
{
    /// <summary>
    /// Utility class providing extension methods to perform queries on noded of the IMifNodeWithChilds type.
    /// </summary>
    public static class QueryExtensions
    {
        /// <summary>
        /// Searches for all MIF nodes that match the given predicate.
        /// </summary>
        public static IEnumerable<IMifNode> FindMifNodes(this IMifNodeWithChilds obj, Func<IMifNode, bool> predicate)
            => obj.Flatten().Where(x => predicate(x));

        /// <summary>
        /// Searches for a MIF node that matches the given predicate.
        /// </summary>
        public static IMifNode FindMifNode(this IMifNodeWithChilds obj, Func<IMifNode, bool> predicate)
            => obj.Flatten().First(x => predicate(x));

        /// <summary>
        /// Searches for a MIF node that matches the given predicate.
        /// </summary>
        public static IMifNode? FindMifNodeOrDefault(this IMifNodeWithChilds obj, Func<IMifNode, bool> predicate)
            => obj.Flatten().FirstOrDefault(x => predicate(x));

        /// <summary>
        /// Searches for all MIF nodes of type T.
        /// </summary>
        public static IEnumerable<T> FindMifNodes<T>(this IMifNodeWithChilds obj) where T : IMifNode
        {
            foreach (var node in obj.Flatten()) { if (node is T tag) { yield return tag; } }
        }

        /// <summary>
        /// Searches for all MIF nodes of type T that match the given predicate.
        /// </summary>
        public static IEnumerable<T> FindMifNodes<T>(this IMifNodeWithChilds obj, Func<T, bool> predicate) where T : IMifNode
            => obj.FindMifNodes<T>().Where(x => predicate(x));

        /// <summary>
        /// Searches for a MIF node of type T that matches the given predicate.
        /// </summary>
        public static T FindMifNode<T>(this IMifNodeWithChilds obj, Func<T, bool> predicate) where T : IMifNode
            => obj.FindMifNodes<T>().First(x => predicate(x));

        /// <summary>
        /// Searches for a MIF node of type T that matches the given predicate.
        /// </summary>
        public static T? FindMifNodeOrDefault<T>(this IMifNodeWithChilds obj, Func<T, bool> predicate) where T : IMifNode
            => obj.FindMifNodes<T>().FirstOrDefault(x => predicate(x));

        /// <summary>
        /// Flattens a hierarchical structure and allows to iterrate over all items in the former hierarchical structure.
        /// </summary>
        public static IEnumerable<IMifNode> Flatten(this IMifNodeWithChilds obj, IEnumerable<IMifNode>? mifNodes = null)
        {
            if (mifNodes is null) { mifNodes = obj.Childs; }
            foreach (var node in mifNodes)
            {
                yield return node;
                if (node is IMifNodeWithChilds nodeWithChilds && nodeWithChilds.Childs.Count > 0)
                {
                    foreach (var childNode in obj.Flatten(nodeWithChilds.Childs)) { yield return childNode; }
                }
            }
        }

        /// <summary>
        /// Searches in all MIF nodes for all MIF nodes of type T.
        /// </summary>
        public static IEnumerable<T> FindMifNodes<T>(this IEnumerable<IMifNodeWithChilds> nodes) where T : IMifNode
        {
            foreach (var node in nodes)
            {
                foreach (var childNode in node.Flatten()) { if (childNode is T tag) { yield return tag; } }
            }
        }

        /// <summary>
        /// Searches in all MIF nodes for all MIF nodes of type T that match the given predicate.
        /// </summary>
        public static IEnumerable<T> FindMifNodes<T>(this IEnumerable<IMifNodeWithChilds> nodes, Func<T, bool> predicate) where T : IMifNode
        {
            return FindMifNodes<T>(nodes).Where(predicate);
        }

    }
}
