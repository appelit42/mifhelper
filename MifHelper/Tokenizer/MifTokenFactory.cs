﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace MifHelper.Tokenizer
{
    /// <summary>
    /// Factory class with helper methods for easier token creation.
    /// Also includes fixed statically defined tokens for easy usage and optimization of memory consumption.
    /// </summary>
    public static class MifTokenFactory
    {
        public static MifToken WhiteSpaceToken { get; } = new(MifTokenType.WhiteSpace, " ");
        public static MifToken LineBreakToken { get; set; } = new(MifTokenType.LineBreak, "\n");
        public static MifToken TagStartToken { get; } = new(MifTokenType.TagStart, "<");
        public static MifToken TagEndToken { get; } = new(MifTokenType.TagEnd, ">");
        public static MifToken BracketStartToken { get; } = new(MifTokenType.BracketStart, "(");
        public static MifToken BracketEndToken { get; } = new(MifTokenType.BracketEnd, ")");
        public static MifToken CommaToken { get; } = new(MifTokenType.Comma, ",");
        public static MifToken EmptyStringToken { get; } = new(MifTokenType.String, "`'");

        public static Dictionary<int, MifToken> IndentCache { get; } = new();
        public static Dictionary<string, MifToken> TagNameCache { get; } = new();

        /// <summary>
        /// Defines the number of spaces that are generated per indentation depth when generating the indentation in the FromIdent method.
        /// </summary>
        public static ushort IndentDepth { get; set; } = 1;

        /// <summary>
        /// Defines the whitespace char used for indentation when generating the indentation in the FromIdent method.
        /// </summary>
        public static char IndentChar { get; set; } = ' ';

        public static ITokenCache? TokenCache { get; set; }

        public static MifToken FromTypeAndValue(MifTokenType mifTokenType, string value) => mifTokenType switch
        {
            MifTokenType.TagStart => TagStartToken,
            MifTokenType.TagEnd => TagEndToken,
            MifTokenType.BracketStart => BracketStartToken,
            MifTokenType.BracketEnd => BracketEndToken,
            MifTokenType.Comma => CommaToken,
            _ => TokenCache?.FromTypeAndValue(mifTokenType, value) ?? new(mifTokenType, value)
        };

        public static MifToken FromTypeAndValue(MifTokenType mifTokenType, ReadOnlySpan<char> value) => mifTokenType switch
        {
            MifTokenType.TagStart => TagStartToken,
            MifTokenType.TagEnd => TagEndToken,
            MifTokenType.BracketStart => BracketStartToken,
            MifTokenType.BracketEnd => BracketEndToken,
            MifTokenType.Comma => CommaToken,
            _ => TokenCache?.FromTypeAndValue(mifTokenType, new string(value)) ?? new(mifTokenType, value)
        };

        /// <summary>
        /// Creates a token representing a string.
        /// </summary>
        /// <param name="stringValue">The string to be converted to a token.</param>
        /// <returns>The token representing the string.</returns>
        public static MifToken FromString(string stringValue)
            => string.IsNullOrEmpty(stringValue) ? EmptyStringToken : new(MifTokenType.String, stringValue[0] == '`' ? stringValue : $"`{stringValue}'");

        /// <summary>
        /// Creates a token representing a comment
        /// </summary>
        /// <param name="comment">The comment string to be coverted to a token.</param>
        /// <returns>The token representing the comment.</returns>
        public static MifToken FromComment(string comment)
            => new(MifTokenType.Comment, comment.StartsWith("#", StringComparison.Ordinal) ? comment : $"#{comment}");

        /// <summary>
        /// Creates a token representing a number.
        /// </summary>
        /// <param name="number">The number to be converted to a token.</param>
        /// <returns>The token representing the number.</returns>
        public static MifToken FromNumber(decimal number)
            => new(MifTokenType.Number, number.ToString(CultureInfo.InvariantCulture));

        /// <summary>
        /// Creates a token representing a decimal number.
        /// </summary>
        /// <param name="decimalNumber">The decimal number to be converted to a token.</param>
        /// <returns>The token representing the decimal number.</returns>
        public static MifToken FromDecimalNumber(decimal decimalNumber)
            => new(MifTokenType.DecimalNumber, decimalNumber.ToString(CultureInfo.InvariantCulture));

        /// <summary>
        /// Creates a token representing the specified indentation level.
        /// </summary>
        /// <param name="indentLevel">The indentation level.</param>
        /// <returns>The token representing the indentation.</returns>
        public static MifToken FromIndent(int indentLevel = 1)
        {
            if (IndentCache.TryGetValue(indentLevel, out var token))
            {
                return token;
            }
            lock (IndentCache)
            {
                return IndentCache[indentLevel] = FromTypeAndValue(MifTokenType.WhiteSpace, new string(IndentChar, indentLevel * IndentDepth));
            }
        }

        /// <summary>
        /// Creates a token representing an identifier.
        /// </summary>
        /// <param name="stringValue">The string to be converted to a token.</param>
        /// <returns>The token representing the string.</returns>
        public static MifToken FromIdentifier(string stringValue)
            => FromTypeAndValue(MifTokenType.Identifier, stringValue);

        /// <summary>
        /// Creates a token representing an tag name.
        /// </summary>
        /// <param name="stringValue">The string to be converted to a token.</param>
        /// <returns>The token representing the string.</returns>
        public static MifToken FromTagName(string tagName)
        {
            if (TagNameCache.TryGetValue(tagName, out var token))
            {
                return token;
            }
            lock (IndentCache)
            {
                return TagNameCache[tagName] = FromIdentifier(tagName);
            }
        }

    }
}
