﻿namespace MifHelper.Tokenizer
{
    /// <summary>
    /// Definiton of the basic MIF token types.
    /// </summary>
    public enum MifTokenType
    {
        None,
        WhiteSpace, // [ \t]*
        LineBreak, // [\r\n]*
        Comment, // #[^\r\n]*

        Identifier, //  ["%]{0,1}[\w]* ( MacroName [\w]* or Keyword [\w]* or Unit ["%]{0,1}[\w]* )
        Number, // [0-9]*
        DecimalNumber, // [0-9]*[\.]{0,1}[0-9]*
        String, // `[^']*'
        Path, // (e.g. c:/home/mif.mif)

        TagStart, // <
        TagEnd, // >
        BracketStart, // (
        BracketEnd, // )
        Comma, // ,
    }
}
