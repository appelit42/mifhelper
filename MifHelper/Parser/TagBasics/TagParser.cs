﻿using MifHelper.Tokenizer;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MifHelper.Parser
{
    /// <summary>
    /// Parser for tags.
    /// </summary>
    public static class TagParser
    {
        /// <summary>
        /// Delegate method that creates a matching implementation for the specified tag name.
        /// </summary>
        /// <param name="tagName">The name of the tag to create.</param>
        /// <param name="parameters">The tokens that represent the parameters of the tag.</param>
        /// <param name="childs">The child nodes.</param>
        /// <returns>The specific tag instance.</returns>
        public delegate IMifTag SpecificTagResolver(string tagName, List<MifToken> parameters, List<IMifNode> childs);

        public static SpecificTagResolver? SpecificTagResolverFunction { get; set; }

        public static IMifTag ParseTag(IList<MifToken> tokens, ref int pos)
        {
            var identifer = tokens.NextToken(ref pos);
            if (identifer.Type != MifTokenType.Identifier) { throw new Exception($"Syntax error (at pos {pos}): Identifier expected!"); }
            var parameters = new List<MifToken>();
            var token = tokens.NextToken(ref pos);
            while (token.Type < MifTokenType.TagStart)
            {
                parameters.Add(token);
                token = tokens.NextToken(ref pos);
            }
            var childs = new List<IMifNode>();
            while (true)
            {
                switch (token.Type)
                {
                    case MifTokenType.WhiteSpace:
                    case MifTokenType.LineBreak:
                        break;
                    case MifTokenType.Comment:
                        childs.Add(new MifComment(token.Value));
                        break;
                    case MifTokenType.Identifier:
                        childs.Add(MacroParser.ParseMacro(tokens, ref pos));
                        break;
                    case MifTokenType.TagStart:
                        childs.Add(ParseTag(tokens, ref pos));
                        break;
                    case MifTokenType.TagEnd:
                        if (childs.Count > 0 || !parameters.Any(x => x.Type != MifTokenType.Comment))
                        {
                            for (int i = parameters.Count - 1; i >= 0 && parameters[i].Type == MifTokenType.Comment; i--)
                            {
                                childs.Insert(0, new MifComment(parameters[i].Value));
                                parameters.RemoveAt(i);
                            }
                        }
                        var tag = SpecificTagResolverFunction?.Invoke(identifer.Value, parameters, childs);
                        tag ??= new MifTag(identifer.Value, parameters, childs);
                        for (int i = 1; pos + i < tokens.Count; i++)
                        {
                            switch (tokens[pos + i].Type)
                            {
                                case MifTokenType.WhiteSpace:
                                    break;
                                case MifTokenType.Comment:
                                    pos += i;
                                    tag.TagEndComment = new MifComment(tokens[pos].Value);
                                    return tag;
                                default:
                                    return tag;
                            }
                        }
                        return tag;
                    default:
                        throw new ArgumentException($"Syntax error (at pos {pos}): Invalid token!");
                }
                token = tokens.NextToken(ref pos);
            }
        }

    }
}
