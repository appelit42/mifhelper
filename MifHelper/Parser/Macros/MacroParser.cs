﻿using MifHelper.Tokenizer;
using System;
using System.Collections.Generic;

namespace MifHelper.Parser
{
    /// <summary>
    /// Parser for macro statements.
    /// </summary>
    public static class MacroParser
    {
        public static IMifMacro ParseMacro(IList<MifToken> tokens, ref int pos)
        {
            var identifer = tokens[pos];
            CheckTokenType(identifer, MifTokenType.Identifier, pos, "Macro statements must start with 'define' or 'include'!");
            return identifer.Value switch
            {
                "define" => ParseDefineMacro(tokens, ref pos),
                "include" => ParseIncludeMacro(tokens, ref pos),
                _ => throw new Exception($"Syntax error (at pos {pos}): { identifer.Value} is not a valid macro name!" +
                                         $" Macro statements must start with 'define' or 'include'!")
            };
        }

        private static MifDefineMacro ParseDefineMacro(IList<MifToken> tokens, ref int pos)
        {
            var bracketStart = tokens.NextToken(ref pos);
            CheckTokenType(bracketStart, MifTokenType.BracketStart, pos, "The macro statements requires a opening bracket!");
            var name = tokens.NextToken(ref pos);
            CheckTokenType(name, MifTokenType.Identifier, pos, "The define macro statements must have a name!");
            var comma = tokens.NextToken(ref pos);
            CheckTokenType(comma, MifTokenType.Comma, pos, "The macro statements requires a comma after the name!");
            var replacement = new List<MifToken>();
            for (++pos; pos < tokens.Count; pos++)
            {
                if (tokens[pos].Type == MifTokenType.BracketEnd)
                {
                    return new MifDefineMacro(name.Value, replacement);
                }
                else
                {
                    replacement.Add(tokens[pos]);
                }
            }
            throw new Exception($"Syntax error (at pos {pos}): Token stream ended before the macro statement closing bracket was found!");
        }

        private static MifIncludeMacro ParseIncludeMacro(IList<MifToken> tokens, ref int pos)
        {
            var bracketStart = tokens.NextToken(ref pos);
            CheckTokenType(bracketStart, MifTokenType.BracketStart, pos, "The macro statements requires a opening bracket!");
            var path = tokens.NextToken(ref pos);
            CheckTokenType(path, MifTokenType.Path, pos, "The include macro statement must contain a path!");
            var bracketEnd = tokens.NextToken(ref pos);
            CheckTokenType(bracketEnd, MifTokenType.BracketEnd, pos, "The macro statements requires a closing bracket!");
            return new MifIncludeMacro(path.Value);
        }

        private static void CheckTokenType(MifToken token, MifTokenType mifTokenType, int pos, string errorMessage)
        {
            if (token.Type != mifTokenType) { throw new Exception($"Syntax error (at pos {pos}): " + errorMessage); }
        }

    }
}
