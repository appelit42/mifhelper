﻿using System.Collections.Generic;

namespace MifHelper.Parser
{
    /// <summary>
    /// Base interface for all elements with childs in the MIF.
    /// </summary>
    public interface IMifNodeWithChilds : IMifNode
    {
        /// <summary>
        /// The list with all child nodes.
        /// </summary>
        public List<IMifNode> Childs { get; }
    }
}
